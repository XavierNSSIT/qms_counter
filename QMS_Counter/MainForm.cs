﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using Newtonsoft.Json;
using System.Configuration;
using QMS_Counter.Models;

namespace QMS_Counter
{
    public partial class MainForm : Form
    {
        private static string counterName = ConfigurationManager.AppSettings["Counter"].ToString();
        private static int getStatusTime = Int32.Parse(ConfigurationManager.AppSettings["GetStatusTime"].ToString());
        private static int buttonDisabledTime = Int32.Parse(ConfigurationManager.AppSettings["ButtonDisabledTime"].ToString());
        private static string apiIp = ConfigurationManager.AppSettings["APIIP"].ToString();
        private static string rgbColor = ConfigurationManager.AppSettings["PanelRGB"].ToString();
        
        public MainForm()
        {
            InitializeComponent();
            Initialize();
        }

        private void Initialize()
        {
            GetQueueStatus(false);

            tmrQuery.Interval = (1000) * (getStatusTime);
            tmrQuery.Enabled = true;
            tmrQuery.Start();

            //string[] colors = rgbColor.Split(',');

            //int colorR = Int32.Parse(colors[0]);
            //int colorG = Int32.Parse(colors[1]);
            //int colorB = Int32.Parse(colors[2]);

            //pnlIndicator_0.BackColor = Color.FromArgb(colorR, colorG, colorB);
            //lblQueueStatus_0.BackColor = Color.FromArgb(colorR, colorG, colorB);
        }

        private void tmrQuery_Tick(object sender, EventArgs e)
        {
            GetQueueStatus(false);
        }
        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }

        private void GetQueueStatus(bool timer)
        {
            GetQueueStatus(timer, false);
        }

        private async void GetQueueStatus(bool timer, bool isReset)
        {
            try
            {
                int count = 0;
                foreach (Control c in this.Controls)
                {
                    if (c.GetType() == typeof(Panel))
                        count++;
                }

                var client = new HttpClient();
                client.BaseAddress = new Uri("http://" + apiIp);
                HttpResponseMessage response = await client.GetAsync("/QMS_API/api/Counter/GetQueueStatus?counter=" + counterName);
                string result = await response.Content.ReadAsStringAsync();

                var model = JsonConvert.DeserializeObject<IList<QueueNoStatusMain>>(result);

                int countResult = 0;

                pnlMain_0.Visible = false;
                pnlMain_1.Visible = false;
                pnlMain_2.Visible = false;
                pnlMain_3.Visible = false;
                pnlMain_4.Visible = false;

                lblQueueStatus_0.Text = "";
                lblQueueStatus_1.Text = "";
                lblQueueStatus_2.Text = "";
                lblQueueStatus_3.Text = "";
                lblQueueStatus_4.Text = "";

                Timer timer1 = tmrBtnNext_0;
                Timer timer2 = tmrBtnNext_1;
                Timer timer3 = tmrBtnNext_2;
                Timer timer4 = tmrBtnNext_3;
                Timer timer5 = tmrBtnNext_4;

                //for (int i = 0; i < model.Count; i++)
                //{
                //    lblQueueStatus.Text += model[i].QueueType + " : " + model[i].TotalQueue.ToString() + "  ,  ";
                //    countResult += model[i].TotalQueue;

                //    if (i == model.Count - 1)
                //    {
                //        lblQueueStatus.Text = lblQueueStatus.Text.Substring(0, lblQueueStatus.Text.Length - 5);
                //    }
                //}

                //if (countResult > 0)
                //{
                //    btnNext.Enabled = true;
                //    btnNext.BackColor = Color.White;
                //    btnNext.Text = "Next";
                //    pnlIndicator.BackColor = Color.Green;

                //}
                //else
                //{
                //    btnNext.Enabled = false;
                //    btnNext.BackColor = Color.FromArgb(175, 175, 175);
                //    btnNext.Text = "";
                //    pnlIndicator.BackColor = Color.Red;
                //    lblQueueStatus.Text = "No More Queue!";
                //}
                for (int i = 0; i < model.Count; i++)
                {
                    if (i < count)
                    {
                        string index = i.ToString();
                        Panel main = this.Controls.Find("pnlMain_" + index, true).FirstOrDefault() as Panel;
                        main.Visible = true;
                        Label queueStatus = this.Controls.Find("lblQueueStatus_" + index, true).FirstOrDefault() as Label;                        
                        Panel indicator = this.Controls.Find("pnlIndicator_" + index, true).FirstOrDefault() as Panel;
                        Label msg1 = this.Controls.Find("lblMsg1_" + index, true).FirstOrDefault() as Label;
                        Label msg2 = this.Controls.Find("lblMsg2_" + index, true).FirstOrDefault() as Label;
                        Label msg3 = this.Controls.Find("lblMsg3_" + index, true).FirstOrDefault() as Label;
                        Label msg4 = this.Controls.Find("lblMsg4_" + index, true).FirstOrDefault() as Label;
                        Button next = this.Controls.Find("btnNext_" + index, true).FirstOrDefault() as Button;
                        Label counterId = this.Controls.Find("lblCounterId_" + index, true).FirstOrDefault() as Label;
                        counterId.Text = model[i].Counter;
                        countResult = 0;
                        
                        for (int j = 0; j < model[i].QueueNoStatus.Count; j++)
                        {
                            queueStatus.Text += model[i].QueueNoStatus[j].QueueType + " : ";

                            if (model[i].QueueNoStatus[j].TotalQueue > 0)
                            {
                                queueStatus.Text += model[i].QueueNoStatus[j].TotalQueue.ToString();
                            }
                            else
                            {
                                queueStatus.Text += "No More Queue!";
                                if (isReset)
                                {
                                    msg1.Text = "-";
                                    msg2.Text = "-";
                                    msg3.Text = "-";
                                    msg4.Text = "";

                                }
                            }

                            queueStatus.Text += "  ,  ";

                            countResult += model[i].QueueNoStatus[j].TotalQueue;
                        }

                        queueStatus.Text = queueStatus.Text.Substring(0, queueStatus.Text.Length - 5);

                        if (countResult > 0)
                        {
                            //next.Enabled = true;
                            //next.BackColor = Color.White;
                            //next.Text = "Next";
                            //pnlIndicator.BackColor = Color.Green;
                            switch (index)
                            {
                                case "0":
                                    if (timer1.Enabled == false)
                                    {
                                        next.Enabled = true;
                                        next.BackColor = Color.White;
                                        next.Text = "Next";
                                    }
                                    break;

                                case "1":
                                    if (timer2.Enabled == false)
                                    {
                                        next.Enabled = true;
                                        next.BackColor = Color.White;
                                        next.Text = "Next";
                                    }
                                    break;

                                case "2":
                                    if (timer3.Enabled == false)
                                    {
                                        next.Enabled = true;
                                        next.BackColor = Color.White;
                                        next.Text = "Next";
                                    }
                                    break;

                                case "3":
                                    if (timer4.Enabled == false)
                                    {
                                        next.Enabled = true;
                                        next.BackColor = Color.White;
                                        next.Text = "Next";
                                    }
                                    break;

                                case "4":
                                    if (timer5.Enabled == false)
                                    {
                                        next.Enabled = true;
                                        next.BackColor = Color.White;
                                        next.Text = "Next";
                                    }
                                    break;

                                default:
                                    break;
                            }
                        }
                        else
                        {
                            switch (index)
                            {
                                case "0":
                                    if (timer1.Enabled == false)
                                    {
                                        next.Enabled = false;
                                        next.Text = "";
                                        next.BackColor = Color.FromArgb(175, 175, 175);
                                    }
                                    break;

                                case "1":
                                    if (timer2.Enabled == false)
                                    {
                                        next.Enabled = false;
                                        next.Text = "";
                                        next.BackColor = Color.FromArgb(175, 175, 175);
                                    }
                                    break;

                                case "2":
                                    if (timer3.Enabled == false)
                                    {
                                        next.Enabled = false;
                                        next.Text = "";
                                        next.BackColor = Color.FromArgb(175, 175, 175);
                                    }
                                    break;

                                case "3":
                                    if (timer4.Enabled == false)
                                    {
                                        next.Enabled = false;
                                        next.Text = "";
                                        next.BackColor = Color.FromArgb(175, 175, 175);
                                    }
                                    break;

                                case "4":
                                    if (timer5.Enabled == false)
                                    {
                                        next.Enabled = false;
                                        next.Text = "";
                                        next.BackColor = Color.FromArgb(175, 175, 175);
                                    }
                                    break;

                                default:
                                    break;
                            }
                            
                            //pnlIndicator.BackColor = Color.Red;
                            //lblQueueStatus.Text = "No More Queue!";
                        }

                        //if (timer)
                        //{
                        //    next.Enabled = false;
                        //    next.Text = "";
                        //    next.BackColor = Color.FromArgb(175, 175, 175);
                        //}
                    }
                }
            }
            catch (Exception e)
            {
                lblQueueStatus_0.Text = e.Message.ToString();
            }
            
        }


        private async void GetNextQueueNo(string index)
        {
            Label counterId = this.Controls.Find("lblCounterId_" + index, true).FirstOrDefault() as Label;
            Label msg1 = this.Controls.Find("lblMsg1_" + index, true).FirstOrDefault() as Label;
            Label msg2 = this.Controls.Find("lblMsg2_" + index, true).FirstOrDefault() as Label;
            Label msg3 = this.Controls.Find("lblMsg3_" + index, true).FirstOrDefault() as Label;
            Label msg4 = this.Controls.Find("lblMsg4_" + index, true).FirstOrDefault() as Label;
            Button next = this.Controls.Find("btnNext_" + index, true).FirstOrDefault() as Button;

            var client = new HttpClient();
            client.BaseAddress = new Uri("http://" + apiIp);
            HttpResponseMessage response = await client.GetAsync("/QMS_API/api/Counter/GetNextQueueNo?counter=" + counterId.Text);
            string result = await response.Content.ReadAsStringAsync();

            try
            {
                GetQueueStatus(true);

                if (result != "null")
                {
                    

                    var model = JsonConvert.DeserializeObject<NextQueueNo>(result);


                    if (msg1.Text != string.Empty)
                    {
                        msg2.Text = msg1.Text;
                    }
                    else
                    {
                        msg2.Text = "-";
                    }

                    msg1.Text = model.QueueNo;
                    msg3.Text = model.DriverName;
                    msg4.Text = " (" + model.PlateNo + ")";
                }
            }
            catch (Exception e)
            { 
                
            }
        }

        //private async void ResetAll()
        //{
        //    try
        //    {
        //        int count = 0;
        //        foreach (Control c in this.Controls)
        //        {
        //            if (c.GetType() == typeof(Panel))
        //                count++;
        //        }

        //        var client = new HttpClient();
        //        client.BaseAddress = new Uri("http://" + apiIp);
        //        HttpResponseMessage response = await client.GetAsync("/QMS_API/api/Counter/GetQueueStatus?counter=" + counterName);
        //        string result = await response.Content.ReadAsStringAsync();

        //        var model = JsonConvert.DeserializeObject<IList<QueueNoStatusMain>>(result);

        //        int countResult = 0;

        //        pnlMain_0.Visible = false;
        //        pnlMain_1.Visible = false;
        //        pnlMain_2.Visible = false;
        //        pnlMain_3.Visible = false;
        //        pnlMain_4.Visible = false;

        //        lblQueueStatus_0.Text = "";
        //        lblQueueStatus_1.Text = "";
        //        lblQueueStatus_2.Text = "";
        //        lblQueueStatus_3.Text = "";
        //        lblQueueStatus_4.Text = "";

        //        Timer timer1 = tmrBtnNext_0;
        //        Timer timer2 = tmrBtnNext_1;
        //        Timer timer3 = tmrBtnNext_2;
        //        Timer timer4 = tmrBtnNext_3;
        //        Timer timer5 = tmrBtnNext_4;

               
        //        for (int i = 0; i < model.Count; i++)
        //        {
        //            if (i < count)
        //            {
        //                string index = i.ToString();
        //                Panel main = this.Controls.Find("pnlMain_" + index, true).FirstOrDefault() as Panel;
        //                main.Visible = true;
        //                Label queueStatus = this.Controls.Find("lblQueueStatus_" + index, true).FirstOrDefault() as Label;
        //                Panel indicator = this.Controls.Find("pnlIndicator_" + index, true).FirstOrDefault() as Panel;
        //                Label msg1 = this.Controls.Find("lblMsg1_" + index, true).FirstOrDefault() as Label;
        //                Label msg2 = this.Controls.Find("lblMsg2_" + index, true).FirstOrDefault() as Label;
        //                Label msg3 = this.Controls.Find("lblMsg3_" + index, true).FirstOrDefault() as Label;
        //                Button next = this.Controls.Find("btnNext_" + index, true).FirstOrDefault() as Button;
        //                Label counterId = this.Controls.Find("lblCounterId_" + index, true).FirstOrDefault() as Label;
        //                counterId.Text = model[i].Counter;
        //                countResult = 0;

        //                for (int j = 0; j < model[i].QueueNoStatus.Count; j++)
        //                {
        //                    queueStatus.Text += model[i].QueueNoStatus[j].QueueType + " : ";

        //                    if (model[i].QueueNoStatus[j].TotalQueue > 0)
        //                    {
        //                        queueStatus.Text += model[i].QueueNoStatus[j].TotalQueue.ToString();
        //                    }
        //                    else
        //                    {
        //                        queueStatus.Text += "No More Queue!";
        //                        msg1.Text = "-";
        //                        msg2.Text = "-";
        //                        msg3.Text = "-";
        //                        msg4.Text = "";

        //                    }

        //                    queueStatus.Text += "  ,  ";

        //                    countResult += model[i].QueueNoStatus[j].TotalQueue;
        //                }

        //                queueStatus.Text = queueStatus.Text.Substring(0, queueStatus.Text.Length - 5);

        //                if (countResult > 0)
        //                {
        //                    //next.Enabled = true;
        //                    //next.BackColor = Color.White;
        //                    //next.Text = "Next";
        //                    //pnlIndicator.BackColor = Color.Green;
        //                    switch (index)
        //                    {
        //                        case "0":
        //                            if (timer1.Enabled == false)
        //                            {
        //                                next.Enabled = true;
        //                                next.BackColor = Color.White;
        //                                next.Text = "Next";
        //                            }
        //                            break;

        //                        case "1":
        //                            if (timer2.Enabled == false)
        //                            {
        //                                next.Enabled = true;
        //                                next.BackColor = Color.White;
        //                                next.Text = "Next";
        //                            }
        //                            break;

        //                        case "2":
        //                            if (timer3.Enabled == false)
        //                            {
        //                                next.Enabled = true;
        //                                next.BackColor = Color.White;
        //                                next.Text = "Next";
        //                            }
        //                            break;

        //                        case "3":
        //                            if (timer4.Enabled == false)
        //                            {
        //                                next.Enabled = true;
        //                                next.BackColor = Color.White;
        //                                next.Text = "Next";
        //                            }
        //                            break;

        //                        case "4":
        //                            if (timer5.Enabled == false)
        //                            {
        //                                next.Enabled = true;
        //                                next.BackColor = Color.White;
        //                                next.Text = "Next";
        //                            }
        //                            break;

        //                        default:
        //                            break;
        //                    }
        //                }
        //                else
        //                {
        //                    switch (index)
        //                    {
        //                        case "0":
        //                            if (timer1.Enabled == false)
        //                            {
        //                                next.Enabled = false;
        //                                next.Text = "";
        //                                next.BackColor = Color.FromArgb(175, 175, 175);
        //                            }
        //                            break;

        //                        case "1":
        //                            if (timer2.Enabled == false)
        //                            {
        //                                next.Enabled = false;
        //                                next.Text = "";
        //                                next.BackColor = Color.FromArgb(175, 175, 175);
        //                            }
        //                            break;

        //                        case "2":
        //                            if (timer3.Enabled == false)
        //                            {
        //                                next.Enabled = false;
        //                                next.Text = "";
        //                                next.BackColor = Color.FromArgb(175, 175, 175);
        //                            }
        //                            break;

        //                        case "3":
        //                            if (timer4.Enabled == false)
        //                            {
        //                                next.Enabled = false;
        //                                next.Text = "";
        //                                next.BackColor = Color.FromArgb(175, 175, 175);
        //                            }
        //                            break;

        //                        case "4":
        //                            if (timer5.Enabled == false)
        //                            {
        //                                next.Enabled = false;
        //                                next.Text = "";
        //                                next.BackColor = Color.FromArgb(175, 175, 175);
        //                            }
        //                            break;

        //                        default:
        //                            break;
        //                    }

        //                    //pnlIndicator.BackColor = Color.Red;
        //                    //lblQueueStatus.Text = "No More Queue!";
        //                }

        //                //if (timer)
        //                //{
        //                //    next.Enabled = false;
        //                //    next.Text = "";
        //                //    next.BackColor = Color.FromArgb(175, 175, 175);
        //                //}
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        lblQueueStatus_0.Text = e.Message.ToString();
        //    }
        //}


        private void btnNext_Click(object sender, EventArgs e)
        {
            GetNextQueueNo("0");
            tmrBtnNext_0.Interval = (1000) * (buttonDisabledTime);
            tmrBtnNext_0.Enabled = true;
            tmrBtnNext_0.Tick += tmrBtnNext_Tick;
            tmrBtnNext_0.Start();
            btnNext_0.Enabled = false;
            btnNext_0.Text = "";
            btnNext_0.BackColor = Color.FromArgb(175, 175, 175);
        }
        
        private void btnNext_1_Click(object sender, EventArgs e)
        {
            GetNextQueueNo("1");
            tmrBtnNext_1.Interval = (1000) * (buttonDisabledTime);
            tmrBtnNext_1.Enabled = true;
            tmrBtnNext_1.Tick += tmrBtnNext_Tick;
            tmrBtnNext_1.Start();
            btnNext_1.Enabled = false;
            btnNext_1.Text = "";
            btnNext_1.BackColor = Color.FromArgb(175, 175, 175);
        }

        private void btnNext_2_Click(object sender, EventArgs e)
        {
            GetNextQueueNo("2");
            tmrBtnNext_2.Interval = (1000) * (buttonDisabledTime);
            tmrBtnNext_2.Enabled = true;
            tmrBtnNext_2.Tick += tmrBtnNext_Tick;
            tmrBtnNext_2.Start();
            btnNext_2.Enabled = false;
            btnNext_2.Text = "";
            btnNext_2.BackColor = Color.FromArgb(175, 175, 175);
        }

        private void btnNext_3_Click(object sender, EventArgs e)
        {
            GetNextQueueNo("3");
            tmrBtnNext_3.Interval = (1000) * (buttonDisabledTime);
            tmrBtnNext_3.Enabled = true;
            tmrBtnNext_3.Tick += tmrBtnNext_Tick;
            tmrBtnNext_3.Start();
            btnNext_3.Enabled = false;
            btnNext_3.Text = "";
            btnNext_3.BackColor = Color.FromArgb(175, 175, 175);
        }

        private void btnNext_4_Click(object sender, EventArgs e)
        {
            GetNextQueueNo("4");
            tmrBtnNext_4.Interval = (1000) * (buttonDisabledTime);
            tmrBtnNext_4.Enabled = true;
            tmrBtnNext_4.Tick += tmrBtnNext_Tick;
            tmrBtnNext_4.Start();
            btnNext_4.Enabled = false;
            btnNext_4.Text = "";
            btnNext_4.BackColor = Color.FromArgb(175, 175, 175);
        }

        private void tmrBtnNext_Tick(object sender, EventArgs e)
        {
            tmrBtnNext_0.Stop();
            tmrBtnNext_0.Enabled = false;
            //btnNext_0.Enabled = true;
        }
        private void tmrBtnNext_1_Tick(object sender, EventArgs e)
        {
            tmrBtnNext_1.Stop();
            tmrBtnNext_1.Enabled = false;
            //btnNext_1.Enabled = true;
        }

        private void tmrBtnNext_2_Tick(object sender, EventArgs e)
        {
            tmrBtnNext_2.Stop();
            tmrBtnNext_2.Enabled = false;
            //btnNext_2.Enabled = true;
        }

        private void tmrBtnNext_3_Tick(object sender, EventArgs e)
        {
            tmrBtnNext_3.Stop();
            tmrBtnNext_3.Enabled = false;
            //btnNext_3.Enabled = true;
        }

        private void tmrBtnNext_4_Tick(object sender, EventArgs e)
        {
            tmrBtnNext_4.Stop();
            tmrBtnNext_4.Enabled = false;
            //btnNext_4.Enabled = true;
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            
            var client = new HttpClient();
            client.BaseAddress = new Uri("http://" + apiIp);
            HttpResponseMessage response = await client.GetAsync("/QMS_API/api/Counter/GetResetNo");
            var result = await response.Content.ReadAsStringAsync();
            var model = JsonConvert.DeserializeObject<ServiceResult>(result);
            try
            {
                if (result != null)
                {
                    if (model.Status == "0")
                    {
                        MessageBox.Show("Queue number reset successfully!", "Queue No Reset", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                        GetQueueStatus(false, true);
                    }
                    else
                    {
                        MessageBox.Show(model.ErrorMessage);
                    }
                }
                else
                {
                    MessageBox.Show("Others Error");
                }
            }

            catch (Exception ex)
            {

            }
        }
    }
}
