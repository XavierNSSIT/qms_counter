﻿namespace QMS_Counter
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnNext_0 = new System.Windows.Forms.Button();
            this.pnlIndicator_0 = new System.Windows.Forms.Panel();
            this.lblQueueStatus_0 = new System.Windows.Forms.Label();
            this.lblMsg2_0 = new System.Windows.Forms.Label();
            this.tmrQuery = new System.Windows.Forms.Timer(this.components);
            this.lblMsg1_0 = new System.Windows.Forms.Label();
            this.lblPreviousServe_0 = new System.Windows.Forms.Label();
            this.lblNowServing_0 = new System.Windows.Forms.Label();
            this.tmrBtnNext_0 = new System.Windows.Forms.Timer(this.components);
            this.pnlMain_0 = new System.Windows.Forms.Panel();
            this.lblMsg3_0 = new System.Windows.Forms.Label();
            this.lblDriverName_0 = new System.Windows.Forms.Label();
            this.lblCounterId_0 = new System.Windows.Forms.Label();
            this.btnNext_2 = new System.Windows.Forms.Button();
            this.pnlIndicator_2 = new System.Windows.Forms.Panel();
            this.lblQueueStatus_2 = new System.Windows.Forms.Label();
            this.lblMsg2_2 = new System.Windows.Forms.Label();
            this.lblNowServing_2 = new System.Windows.Forms.Label();
            this.lblPreviousServe_2 = new System.Windows.Forms.Label();
            this.lblMsg1_2 = new System.Windows.Forms.Label();
            this.tmrBtnNext_1 = new System.Windows.Forms.Timer(this.components);
            this.pnlMain_2 = new System.Windows.Forms.Panel();
            this.lblMsg3_2 = new System.Windows.Forms.Label();
            this.lblDriverName_2 = new System.Windows.Forms.Label();
            this.lblCounterId_2 = new System.Windows.Forms.Label();
            this.btnNext_3 = new System.Windows.Forms.Button();
            this.pnlIndicator_3 = new System.Windows.Forms.Panel();
            this.lblQueueStatus_3 = new System.Windows.Forms.Label();
            this.lblMsg2_3 = new System.Windows.Forms.Label();
            this.lblNowServing_3 = new System.Windows.Forms.Label();
            this.lblPreviousServe_3 = new System.Windows.Forms.Label();
            this.lblMsg1_3 = new System.Windows.Forms.Label();
            this.tmrBtnNext_2 = new System.Windows.Forms.Timer(this.components);
            this.pnlMain_3 = new System.Windows.Forms.Panel();
            this.lblMsg3_3 = new System.Windows.Forms.Label();
            this.lblDriverName_3 = new System.Windows.Forms.Label();
            this.lblCounterId_3 = new System.Windows.Forms.Label();
            this.btnNext_4 = new System.Windows.Forms.Button();
            this.pnlIndicator_4 = new System.Windows.Forms.Panel();
            this.lblQueueStatus_4 = new System.Windows.Forms.Label();
            this.lblMsg2_4 = new System.Windows.Forms.Label();
            this.lblNowServing_4 = new System.Windows.Forms.Label();
            this.lblPreviousServe_4 = new System.Windows.Forms.Label();
            this.lblMsg1_4 = new System.Windows.Forms.Label();
            this.tmrBtnNext_3 = new System.Windows.Forms.Timer(this.components);
            this.pnlMain_4 = new System.Windows.Forms.Panel();
            this.lblMsg3_4 = new System.Windows.Forms.Label();
            this.lblDriverName_4 = new System.Windows.Forms.Label();
            this.lblCounterId_4 = new System.Windows.Forms.Label();
            this.btnNext_1 = new System.Windows.Forms.Button();
            this.pnlIndicator_1 = new System.Windows.Forms.Panel();
            this.lblQueueStatus_1 = new System.Windows.Forms.Label();
            this.lblMsg2_1 = new System.Windows.Forms.Label();
            this.lblNowServing_1 = new System.Windows.Forms.Label();
            this.lblPreviousServe_1 = new System.Windows.Forms.Label();
            this.lblMsg1_1 = new System.Windows.Forms.Label();
            this.tmrBtnNext_4 = new System.Windows.Forms.Timer(this.components);
            this.pnlMain_1 = new System.Windows.Forms.Panel();
            this.lblMsg3_1 = new System.Windows.Forms.Label();
            this.lblDriverName_1 = new System.Windows.Forms.Label();
            this.lblCounterId_1 = new System.Windows.Forms.Label();
            this.reset = new System.Windows.Forms.Button();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.lblMsg4_1 = new System.Windows.Forms.Label();
            this.lblMsg4_0 = new System.Windows.Forms.Label();
            this.lblMsg4_2 = new System.Windows.Forms.Label();
            this.lblMsg4_3 = new System.Windows.Forms.Label();
            this.lblMsg4_4 = new System.Windows.Forms.Label();
            this.pnlIndicator_0.SuspendLayout();
            this.pnlMain_0.SuspendLayout();
            this.pnlIndicator_2.SuspendLayout();
            this.pnlMain_2.SuspendLayout();
            this.pnlIndicator_3.SuspendLayout();
            this.pnlMain_3.SuspendLayout();
            this.pnlIndicator_4.SuspendLayout();
            this.pnlMain_4.SuspendLayout();
            this.pnlIndicator_1.SuspendLayout();
            this.pnlMain_1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnNext_0
            // 
            this.btnNext_0.BackColor = System.Drawing.Color.White;
            this.btnNext_0.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnNext_0.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNext_0.Font = new System.Drawing.Font("Cooper Black", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext_0.ForeColor = System.Drawing.Color.Black;
            this.btnNext_0.Location = new System.Drawing.Point(-2, 112);
            this.btnNext_0.Name = "btnNext_0";
            this.btnNext_0.Size = new System.Drawing.Size(316, 43);
            this.btnNext_0.TabIndex = 3;
            this.btnNext_0.Text = "Next";
            this.btnNext_0.UseVisualStyleBackColor = false;
            this.btnNext_0.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // pnlIndicator_0
            // 
            this.pnlIndicator_0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(12)))), ((int)(((byte)(56)))));
            this.pnlIndicator_0.Controls.Add(this.lblQueueStatus_0);
            this.pnlIndicator_0.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlIndicator_0.Location = new System.Drawing.Point(-1, -1);
            this.pnlIndicator_0.Name = "pnlIndicator_0";
            this.pnlIndicator_0.Size = new System.Drawing.Size(315, 43);
            this.pnlIndicator_0.TabIndex = 1;
            // 
            // lblQueueStatus_0
            // 
            this.lblQueueStatus_0.AutoSize = true;
            this.lblQueueStatus_0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(12)))), ((int)(((byte)(56)))));
            this.lblQueueStatus_0.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.lblQueueStatus_0.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblQueueStatus_0.Location = new System.Drawing.Point(2, 11);
            this.lblQueueStatus_0.Name = "lblQueueStatus_0";
            this.lblQueueStatus_0.Size = new System.Drawing.Size(0, 24);
            this.lblQueueStatus_0.TabIndex = 0;
            // 
            // lblMsg2_0
            // 
            this.lblMsg2_0.AutoSize = true;
            this.lblMsg2_0.Font = new System.Drawing.Font("Cambria", 8F);
            this.lblMsg2_0.ForeColor = System.Drawing.Color.Black;
            this.lblMsg2_0.Location = new System.Drawing.Point(99, 92);
            this.lblMsg2_0.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg2_0.Name = "lblMsg2_0";
            this.lblMsg2_0.Size = new System.Drawing.Size(9, 12);
            this.lblMsg2_0.TabIndex = 2;
            this.lblMsg2_0.Text = "-";
            // 
            // tmrQuery
            // 
            this.tmrQuery.Tick += new System.EventHandler(this.tmrQuery_Tick);
            // 
            // lblMsg1_0
            // 
            this.lblMsg1_0.AutoSize = true;
            this.lblMsg1_0.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg1_0.ForeColor = System.Drawing.Color.Black;
            this.lblMsg1_0.Location = new System.Drawing.Point(115, 48);
            this.lblMsg1_0.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg1_0.Name = "lblMsg1_0";
            this.lblMsg1_0.Size = new System.Drawing.Size(16, 22);
            this.lblMsg1_0.TabIndex = 4;
            this.lblMsg1_0.Text = "-";
            // 
            // lblPreviousServe_0
            // 
            this.lblPreviousServe_0.AutoSize = true;
            this.lblPreviousServe_0.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreviousServe_0.ForeColor = System.Drawing.Color.Black;
            this.lblPreviousServe_0.Location = new System.Drawing.Point(5, 92);
            this.lblPreviousServe_0.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblPreviousServe_0.Name = "lblPreviousServe_0";
            this.lblPreviousServe_0.Size = new System.Drawing.Size(88, 12);
            this.lblPreviousServe_0.TabIndex = 5;
            this.lblPreviousServe_0.Text = "Previous Served : ";
            // 
            // lblNowServing_0
            // 
            this.lblNowServing_0.AutoSize = true;
            this.lblNowServing_0.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNowServing_0.ForeColor = System.Drawing.Color.Black;
            this.lblNowServing_0.Location = new System.Drawing.Point(3, 49);
            this.lblNowServing_0.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblNowServing_0.Name = "lblNowServing_0";
            this.lblNowServing_0.Size = new System.Drawing.Size(112, 19);
            this.lblNowServing_0.TabIndex = 6;
            this.lblNowServing_0.Text = "Now Serving : ";
            // 
            // tmrBtnNext_0
            // 
            this.tmrBtnNext_0.Tag = global::QMS_Counter.Properties.Settings.Default.timer_0;
            this.tmrBtnNext_0.Tick += new System.EventHandler(this.tmrBtnNext_Tick);
            // 
            // pnlMain_0
            // 
            this.pnlMain_0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(198)))), ((int)(((byte)(198)))));
            this.pnlMain_0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMain_0.Controls.Add(this.lblMsg4_0);
            this.pnlMain_0.Controls.Add(this.lblMsg3_0);
            this.pnlMain_0.Controls.Add(this.lblDriverName_0);
            this.pnlMain_0.Controls.Add(this.lblCounterId_0);
            this.pnlMain_0.Controls.Add(this.btnNext_0);
            this.pnlMain_0.Controls.Add(this.lblNowServing_0);
            this.pnlMain_0.Controls.Add(this.pnlIndicator_0);
            this.pnlMain_0.Controls.Add(this.lblPreviousServe_0);
            this.pnlMain_0.Controls.Add(this.lblMsg2_0);
            this.pnlMain_0.Controls.Add(this.lblMsg1_0);
            this.pnlMain_0.Location = new System.Drawing.Point(12, 11);
            this.pnlMain_0.Name = "pnlMain_0";
            this.pnlMain_0.Size = new System.Drawing.Size(315, 156);
            this.pnlMain_0.TabIndex = 7;
            // 
            // lblMsg3_0
            // 
            this.lblMsg3_0.AutoSize = true;
            this.lblMsg3_0.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg3_0.ForeColor = System.Drawing.Color.Black;
            this.lblMsg3_0.Location = new System.Drawing.Point(77, 71);
            this.lblMsg3_0.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg3_0.Name = "lblMsg3_0";
            this.lblMsg3_0.Size = new System.Drawing.Size(9, 12);
            this.lblMsg3_0.TabIndex = 9;
            this.lblMsg3_0.Text = "-";
            // 
            // lblDriverName_0
            // 
            this.lblDriverName_0.AutoSize = true;
            this.lblDriverName_0.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDriverName_0.ForeColor = System.Drawing.Color.Black;
            this.lblDriverName_0.Location = new System.Drawing.Point(5, 71);
            this.lblDriverName_0.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblDriverName_0.Name = "lblDriverName_0";
            this.lblDriverName_0.Size = new System.Drawing.Size(72, 12);
            this.lblDriverName_0.TabIndex = 8;
            this.lblDriverName_0.Text = "Driver Name:";
            // 
            // lblCounterId_0
            // 
            this.lblCounterId_0.AutoSize = true;
            this.lblCounterId_0.Location = new System.Drawing.Point(277, 55);
            this.lblCounterId_0.Name = "lblCounterId_0";
            this.lblCounterId_0.Size = new System.Drawing.Size(35, 13);
            this.lblCounterId_0.TabIndex = 7;
            this.lblCounterId_0.Text = "label1";
            this.lblCounterId_0.Visible = false;
            // 
            // btnNext_2
            // 
            this.btnNext_2.BackColor = System.Drawing.Color.White;
            this.btnNext_2.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnNext_2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNext_2.Font = new System.Drawing.Font("Cooper Black", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext_2.ForeColor = System.Drawing.Color.Black;
            this.btnNext_2.Location = new System.Drawing.Point(-1, 112);
            this.btnNext_2.Name = "btnNext_2";
            this.btnNext_2.Size = new System.Drawing.Size(316, 43);
            this.btnNext_2.TabIndex = 3;
            this.btnNext_2.Text = "Next";
            this.btnNext_2.UseVisualStyleBackColor = false;
            this.btnNext_2.Click += new System.EventHandler(this.btnNext_2_Click);
            // 
            // pnlIndicator_2
            // 
            this.pnlIndicator_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(18)))), ((int)(((byte)(120)))));
            this.pnlIndicator_2.Controls.Add(this.lblQueueStatus_2);
            this.pnlIndicator_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlIndicator_2.Location = new System.Drawing.Point(-1, -1);
            this.pnlIndicator_2.Name = "pnlIndicator_2";
            this.pnlIndicator_2.Size = new System.Drawing.Size(316, 43);
            this.pnlIndicator_2.TabIndex = 1;
            // 
            // lblQueueStatus_2
            // 
            this.lblQueueStatus_2.AutoSize = true;
            this.lblQueueStatus_2.BackColor = this.pnlIndicator_2.BackColor;
            this.lblQueueStatus_2.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.lblQueueStatus_2.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblQueueStatus_2.Location = new System.Drawing.Point(2, 11);
            this.lblQueueStatus_2.Name = "lblQueueStatus_2";
            this.lblQueueStatus_2.Size = new System.Drawing.Size(0, 24);
            this.lblQueueStatus_2.TabIndex = 0;
            // 
            // lblMsg2_2
            // 
            this.lblMsg2_2.AutoSize = true;
            this.lblMsg2_2.Font = new System.Drawing.Font("Cambria", 8F);
            this.lblMsg2_2.ForeColor = System.Drawing.Color.Black;
            this.lblMsg2_2.Location = new System.Drawing.Point(99, 92);
            this.lblMsg2_2.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg2_2.Name = "lblMsg2_2";
            this.lblMsg2_2.Size = new System.Drawing.Size(9, 12);
            this.lblMsg2_2.TabIndex = 2;
            this.lblMsg2_2.Text = "-";
            // 
            // lblNowServing_2
            // 
            this.lblNowServing_2.AutoSize = true;
            this.lblNowServing_2.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNowServing_2.ForeColor = System.Drawing.Color.Black;
            this.lblNowServing_2.Location = new System.Drawing.Point(3, 49);
            this.lblNowServing_2.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblNowServing_2.Name = "lblNowServing_2";
            this.lblNowServing_2.Size = new System.Drawing.Size(112, 19);
            this.lblNowServing_2.TabIndex = 6;
            this.lblNowServing_2.Text = "Now Serving : ";
            // 
            // lblPreviousServe_2
            // 
            this.lblPreviousServe_2.AutoSize = true;
            this.lblPreviousServe_2.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreviousServe_2.ForeColor = System.Drawing.Color.Black;
            this.lblPreviousServe_2.Location = new System.Drawing.Point(5, 92);
            this.lblPreviousServe_2.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblPreviousServe_2.Name = "lblPreviousServe_2";
            this.lblPreviousServe_2.Size = new System.Drawing.Size(88, 12);
            this.lblPreviousServe_2.TabIndex = 5;
            this.lblPreviousServe_2.Text = "Previous Served : ";
            // 
            // lblMsg1_2
            // 
            this.lblMsg1_2.AutoSize = true;
            this.lblMsg1_2.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg1_2.ForeColor = System.Drawing.Color.Black;
            this.lblMsg1_2.Location = new System.Drawing.Point(115, 48);
            this.lblMsg1_2.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg1_2.Name = "lblMsg1_2";
            this.lblMsg1_2.Size = new System.Drawing.Size(16, 22);
            this.lblMsg1_2.TabIndex = 4;
            this.lblMsg1_2.Text = "-";
            // 
            // tmrBtnNext_1
            // 
            this.tmrBtnNext_1.Tag = global::QMS_Counter.Properties.Settings.Default.timer_1;
            this.tmrBtnNext_1.Tick += new System.EventHandler(this.tmrBtnNext_1_Tick);
            // 
            // pnlMain_2
            // 
            this.pnlMain_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(209)))), ((int)(((byte)(255)))));
            this.pnlMain_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMain_2.Controls.Add(this.lblMsg4_2);
            this.pnlMain_2.Controls.Add(this.lblMsg3_2);
            this.pnlMain_2.Controls.Add(this.lblDriverName_2);
            this.pnlMain_2.Controls.Add(this.lblCounterId_2);
            this.pnlMain_2.Controls.Add(this.btnNext_2);
            this.pnlMain_2.Controls.Add(this.lblNowServing_2);
            this.pnlMain_2.Controls.Add(this.pnlIndicator_2);
            this.pnlMain_2.Controls.Add(this.lblPreviousServe_2);
            this.pnlMain_2.Controls.Add(this.lblMsg2_2);
            this.pnlMain_2.Controls.Add(this.lblMsg1_2);
            this.pnlMain_2.Location = new System.Drawing.Point(12, 180);
            this.pnlMain_2.Name = "pnlMain_2";
            this.pnlMain_2.Size = new System.Drawing.Size(315, 156);
            this.pnlMain_2.TabIndex = 8;
            // 
            // lblMsg3_2
            // 
            this.lblMsg3_2.AutoSize = true;
            this.lblMsg3_2.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg3_2.ForeColor = System.Drawing.Color.Black;
            this.lblMsg3_2.Location = new System.Drawing.Point(77, 71);
            this.lblMsg3_2.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg3_2.Name = "lblMsg3_2";
            this.lblMsg3_2.Size = new System.Drawing.Size(9, 12);
            this.lblMsg3_2.TabIndex = 10;
            this.lblMsg3_2.Text = "-";
            // 
            // lblDriverName_2
            // 
            this.lblDriverName_2.AutoSize = true;
            this.lblDriverName_2.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDriverName_2.ForeColor = System.Drawing.Color.Black;
            this.lblDriverName_2.Location = new System.Drawing.Point(5, 71);
            this.lblDriverName_2.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblDriverName_2.Name = "lblDriverName_2";
            this.lblDriverName_2.Size = new System.Drawing.Size(72, 12);
            this.lblDriverName_2.TabIndex = 9;
            this.lblDriverName_2.Text = "Driver Name:";
            // 
            // lblCounterId_2
            // 
            this.lblCounterId_2.AutoSize = true;
            this.lblCounterId_2.Location = new System.Drawing.Point(277, 57);
            this.lblCounterId_2.Name = "lblCounterId_2";
            this.lblCounterId_2.Size = new System.Drawing.Size(35, 13);
            this.lblCounterId_2.TabIndex = 8;
            this.lblCounterId_2.Text = "label1";
            this.lblCounterId_2.Visible = false;
            // 
            // btnNext_3
            // 
            this.btnNext_3.BackColor = System.Drawing.Color.White;
            this.btnNext_3.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnNext_3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNext_3.Font = new System.Drawing.Font("Cooper Black", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext_3.ForeColor = System.Drawing.Color.Black;
            this.btnNext_3.Location = new System.Drawing.Point(-1, 110);
            this.btnNext_3.Name = "btnNext_3";
            this.btnNext_3.Size = new System.Drawing.Size(316, 43);
            this.btnNext_3.TabIndex = 3;
            this.btnNext_3.Text = "Next";
            this.btnNext_3.UseVisualStyleBackColor = false;
            this.btnNext_3.Click += new System.EventHandler(this.btnNext_3_Click);
            // 
            // pnlIndicator_3
            // 
            this.pnlIndicator_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(156)))), ((int)(((byte)(39)))));
            this.pnlIndicator_3.Controls.Add(this.lblQueueStatus_3);
            this.pnlIndicator_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlIndicator_3.Location = new System.Drawing.Point(-1, -1);
            this.pnlIndicator_3.Name = "pnlIndicator_3";
            this.pnlIndicator_3.Size = new System.Drawing.Size(316, 43);
            this.pnlIndicator_3.TabIndex = 1;
            // 
            // lblQueueStatus_3
            // 
            this.lblQueueStatus_3.AutoSize = true;
            this.lblQueueStatus_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(156)))), ((int)(((byte)(39)))));
            this.lblQueueStatus_3.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.lblQueueStatus_3.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblQueueStatus_3.Location = new System.Drawing.Point(2, 11);
            this.lblQueueStatus_3.Name = "lblQueueStatus_3";
            this.lblQueueStatus_3.Size = new System.Drawing.Size(0, 24);
            this.lblQueueStatus_3.TabIndex = 0;
            // 
            // lblMsg2_3
            // 
            this.lblMsg2_3.AutoSize = true;
            this.lblMsg2_3.Font = new System.Drawing.Font("Cambria", 8F);
            this.lblMsg2_3.ForeColor = System.Drawing.Color.Black;
            this.lblMsg2_3.Location = new System.Drawing.Point(99, 92);
            this.lblMsg2_3.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg2_3.Name = "lblMsg2_3";
            this.lblMsg2_3.Size = new System.Drawing.Size(9, 12);
            this.lblMsg2_3.TabIndex = 2;
            this.lblMsg2_3.Text = "-";
            // 
            // lblNowServing_3
            // 
            this.lblNowServing_3.AutoSize = true;
            this.lblNowServing_3.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNowServing_3.ForeColor = System.Drawing.Color.Black;
            this.lblNowServing_3.Location = new System.Drawing.Point(3, 49);
            this.lblNowServing_3.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblNowServing_3.Name = "lblNowServing_3";
            this.lblNowServing_3.Size = new System.Drawing.Size(112, 19);
            this.lblNowServing_3.TabIndex = 6;
            this.lblNowServing_3.Text = "Now Serving : ";
            // 
            // lblPreviousServe_3
            // 
            this.lblPreviousServe_3.AutoSize = true;
            this.lblPreviousServe_3.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreviousServe_3.ForeColor = System.Drawing.Color.Black;
            this.lblPreviousServe_3.Location = new System.Drawing.Point(5, 92);
            this.lblPreviousServe_3.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblPreviousServe_3.Name = "lblPreviousServe_3";
            this.lblPreviousServe_3.Size = new System.Drawing.Size(88, 12);
            this.lblPreviousServe_3.TabIndex = 5;
            this.lblPreviousServe_3.Text = "Previous Served : ";
            // 
            // lblMsg1_3
            // 
            this.lblMsg1_3.AutoSize = true;
            this.lblMsg1_3.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg1_3.ForeColor = System.Drawing.Color.Black;
            this.lblMsg1_3.Location = new System.Drawing.Point(115, 48);
            this.lblMsg1_3.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg1_3.Name = "lblMsg1_3";
            this.lblMsg1_3.Size = new System.Drawing.Size(16, 22);
            this.lblMsg1_3.TabIndex = 4;
            this.lblMsg1_3.Text = "-";
            // 
            // tmrBtnNext_2
            // 
            this.tmrBtnNext_2.Tag = global::QMS_Counter.Properties.Settings.Default.timer_2;
            this.tmrBtnNext_2.Tick += new System.EventHandler(this.tmrBtnNext_2_Tick);
            // 
            // pnlMain_3
            // 
            this.pnlMain_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(248)))), ((int)(((byte)(196)))));
            this.pnlMain_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMain_3.Controls.Add(this.lblMsg4_3);
            this.pnlMain_3.Controls.Add(this.lblMsg3_3);
            this.pnlMain_3.Controls.Add(this.lblDriverName_3);
            this.pnlMain_3.Controls.Add(this.lblCounterId_3);
            this.pnlMain_3.Controls.Add(this.btnNext_3);
            this.pnlMain_3.Controls.Add(this.lblNowServing_3);
            this.pnlMain_3.Controls.Add(this.pnlIndicator_3);
            this.pnlMain_3.Controls.Add(this.lblPreviousServe_3);
            this.pnlMain_3.Controls.Add(this.lblMsg2_3);
            this.pnlMain_3.Controls.Add(this.lblMsg1_3);
            this.pnlMain_3.Location = new System.Drawing.Point(340, 181);
            this.pnlMain_3.Name = "pnlMain_3";
            this.pnlMain_3.Size = new System.Drawing.Size(315, 154);
            this.pnlMain_3.TabIndex = 8;
            // 
            // lblMsg3_3
            // 
            this.lblMsg3_3.AutoSize = true;
            this.lblMsg3_3.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg3_3.ForeColor = System.Drawing.Color.Black;
            this.lblMsg3_3.Location = new System.Drawing.Point(77, 71);
            this.lblMsg3_3.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg3_3.Name = "lblMsg3_3";
            this.lblMsg3_3.Size = new System.Drawing.Size(9, 12);
            this.lblMsg3_3.TabIndex = 10;
            this.lblMsg3_3.Text = "-";
            // 
            // lblDriverName_3
            // 
            this.lblDriverName_3.AutoSize = true;
            this.lblDriverName_3.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDriverName_3.ForeColor = System.Drawing.Color.Black;
            this.lblDriverName_3.Location = new System.Drawing.Point(5, 71);
            this.lblDriverName_3.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblDriverName_3.Name = "lblDriverName_3";
            this.lblDriverName_3.Size = new System.Drawing.Size(72, 12);
            this.lblDriverName_3.TabIndex = 9;
            this.lblDriverName_3.Text = "Driver Name:";
            // 
            // lblCounterId_3
            // 
            this.lblCounterId_3.AutoSize = true;
            this.lblCounterId_3.Location = new System.Drawing.Point(276, 57);
            this.lblCounterId_3.Name = "lblCounterId_3";
            this.lblCounterId_3.Size = new System.Drawing.Size(35, 13);
            this.lblCounterId_3.TabIndex = 8;
            this.lblCounterId_3.Text = "label1";
            this.lblCounterId_3.Visible = false;
            // 
            // btnNext_4
            // 
            this.btnNext_4.BackColor = System.Drawing.Color.White;
            this.btnNext_4.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnNext_4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNext_4.Font = new System.Drawing.Font("Cooper Black", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext_4.ForeColor = System.Drawing.Color.Black;
            this.btnNext_4.Location = new System.Drawing.Point(-1, 112);
            this.btnNext_4.Name = "btnNext_4";
            this.btnNext_4.Size = new System.Drawing.Size(316, 43);
            this.btnNext_4.TabIndex = 3;
            this.btnNext_4.Text = "Next";
            this.btnNext_4.UseVisualStyleBackColor = false;
            this.btnNext_4.Click += new System.EventHandler(this.btnNext_4_Click);
            // 
            // pnlIndicator_4
            // 
            this.pnlIndicator_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(0)))), ((int)(((byte)(82)))));
            this.pnlIndicator_4.Controls.Add(this.lblQueueStatus_4);
            this.pnlIndicator_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlIndicator_4.Location = new System.Drawing.Point(-1, -1);
            this.pnlIndicator_4.Name = "pnlIndicator_4";
            this.pnlIndicator_4.Size = new System.Drawing.Size(316, 43);
            this.pnlIndicator_4.TabIndex = 1;
            // 
            // lblQueueStatus_4
            // 
            this.lblQueueStatus_4.AutoSize = true;
            this.lblQueueStatus_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(0)))), ((int)(((byte)(82)))));
            this.lblQueueStatus_4.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.lblQueueStatus_4.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblQueueStatus_4.Location = new System.Drawing.Point(2, 11);
            this.lblQueueStatus_4.Name = "lblQueueStatus_4";
            this.lblQueueStatus_4.Size = new System.Drawing.Size(0, 24);
            this.lblQueueStatus_4.TabIndex = 0;
            // 
            // lblMsg2_4
            // 
            this.lblMsg2_4.AutoSize = true;
            this.lblMsg2_4.Font = new System.Drawing.Font("Cambria", 8F);
            this.lblMsg2_4.ForeColor = System.Drawing.Color.Black;
            this.lblMsg2_4.Location = new System.Drawing.Point(99, 92);
            this.lblMsg2_4.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg2_4.Name = "lblMsg2_4";
            this.lblMsg2_4.Size = new System.Drawing.Size(9, 12);
            this.lblMsg2_4.TabIndex = 2;
            this.lblMsg2_4.Text = "-";
            // 
            // lblNowServing_4
            // 
            this.lblNowServing_4.AutoSize = true;
            this.lblNowServing_4.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNowServing_4.ForeColor = System.Drawing.Color.Black;
            this.lblNowServing_4.Location = new System.Drawing.Point(3, 49);
            this.lblNowServing_4.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblNowServing_4.Name = "lblNowServing_4";
            this.lblNowServing_4.Size = new System.Drawing.Size(112, 19);
            this.lblNowServing_4.TabIndex = 6;
            this.lblNowServing_4.Text = "Now Serving : ";
            // 
            // lblPreviousServe_4
            // 
            this.lblPreviousServe_4.AutoSize = true;
            this.lblPreviousServe_4.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreviousServe_4.ForeColor = System.Drawing.Color.Black;
            this.lblPreviousServe_4.Location = new System.Drawing.Point(5, 92);
            this.lblPreviousServe_4.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblPreviousServe_4.Name = "lblPreviousServe_4";
            this.lblPreviousServe_4.Size = new System.Drawing.Size(88, 12);
            this.lblPreviousServe_4.TabIndex = 5;
            this.lblPreviousServe_4.Text = "Previous Served : ";
            // 
            // lblMsg1_4
            // 
            this.lblMsg1_4.AutoSize = true;
            this.lblMsg1_4.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg1_4.ForeColor = System.Drawing.Color.Black;
            this.lblMsg1_4.Location = new System.Drawing.Point(115, 48);
            this.lblMsg1_4.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg1_4.Name = "lblMsg1_4";
            this.lblMsg1_4.Size = new System.Drawing.Size(16, 22);
            this.lblMsg1_4.TabIndex = 4;
            this.lblMsg1_4.Text = "-";
            // 
            // tmrBtnNext_3
            // 
            this.tmrBtnNext_3.Tag = global::QMS_Counter.Properties.Settings.Default.timer_3;
            this.tmrBtnNext_3.Tick += new System.EventHandler(this.tmrBtnNext_3_Tick);
            // 
            // pnlMain_4
            // 
            this.pnlMain_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(150)))), ((int)(((byte)(232)))));
            this.pnlMain_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMain_4.Controls.Add(this.lblMsg4_4);
            this.pnlMain_4.Controls.Add(this.lblMsg3_4);
            this.pnlMain_4.Controls.Add(this.lblDriverName_4);
            this.pnlMain_4.Controls.Add(this.lblCounterId_4);
            this.pnlMain_4.Controls.Add(this.btnNext_4);
            this.pnlMain_4.Controls.Add(this.lblNowServing_4);
            this.pnlMain_4.Controls.Add(this.pnlIndicator_4);
            this.pnlMain_4.Controls.Add(this.lblPreviousServe_4);
            this.pnlMain_4.Controls.Add(this.lblMsg2_4);
            this.pnlMain_4.Controls.Add(this.lblMsg1_4);
            this.pnlMain_4.Location = new System.Drawing.Point(170, 344);
            this.pnlMain_4.Name = "pnlMain_4";
            this.pnlMain_4.Size = new System.Drawing.Size(316, 156);
            this.pnlMain_4.TabIndex = 8;
            // 
            // lblMsg3_4
            // 
            this.lblMsg3_4.AutoSize = true;
            this.lblMsg3_4.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg3_4.ForeColor = System.Drawing.Color.Black;
            this.lblMsg3_4.Location = new System.Drawing.Point(77, 71);
            this.lblMsg3_4.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg3_4.Name = "lblMsg3_4";
            this.lblMsg3_4.Size = new System.Drawing.Size(9, 12);
            this.lblMsg3_4.TabIndex = 10;
            this.lblMsg3_4.Text = "-";
            // 
            // lblDriverName_4
            // 
            this.lblDriverName_4.AutoSize = true;
            this.lblDriverName_4.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDriverName_4.ForeColor = System.Drawing.Color.Black;
            this.lblDriverName_4.Location = new System.Drawing.Point(5, 71);
            this.lblDriverName_4.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblDriverName_4.Name = "lblDriverName_4";
            this.lblDriverName_4.Size = new System.Drawing.Size(72, 12);
            this.lblDriverName_4.TabIndex = 9;
            this.lblDriverName_4.Text = "Driver Name:";
            // 
            // lblCounterId_4
            // 
            this.lblCounterId_4.AutoSize = true;
            this.lblCounterId_4.Location = new System.Drawing.Point(277, 57);
            this.lblCounterId_4.Name = "lblCounterId_4";
            this.lblCounterId_4.Size = new System.Drawing.Size(35, 13);
            this.lblCounterId_4.TabIndex = 8;
            this.lblCounterId_4.Text = "label1";
            this.lblCounterId_4.Visible = false;
            // 
            // btnNext_1
            // 
            this.btnNext_1.BackColor = System.Drawing.Color.White;
            this.btnNext_1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnNext_1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNext_1.Font = new System.Drawing.Font("Cooper Black", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext_1.ForeColor = System.Drawing.Color.Black;
            this.btnNext_1.Location = new System.Drawing.Point(-1, 112);
            this.btnNext_1.Name = "btnNext_1";
            this.btnNext_1.Size = new System.Drawing.Size(317, 43);
            this.btnNext_1.TabIndex = 3;
            this.btnNext_1.Text = "Next";
            this.btnNext_1.UseVisualStyleBackColor = false;
            this.btnNext_1.Click += new System.EventHandler(this.btnNext_1_Click);
            // 
            // pnlIndicator_1
            // 
            this.pnlIndicator_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(138)))), ((int)(((byte)(0)))));
            this.pnlIndicator_1.Controls.Add(this.lblQueueStatus_1);
            this.pnlIndicator_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlIndicator_1.Location = new System.Drawing.Point(-1, -1);
            this.pnlIndicator_1.Name = "pnlIndicator_1";
            this.pnlIndicator_1.Size = new System.Drawing.Size(316, 43);
            this.pnlIndicator_1.TabIndex = 1;
            // 
            // lblQueueStatus_1
            // 
            this.lblQueueStatus_1.AutoSize = true;
            this.lblQueueStatus_1.BackColor = this.pnlIndicator_1.BackColor;
            this.lblQueueStatus_1.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.lblQueueStatus_1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblQueueStatus_1.Location = new System.Drawing.Point(2, 11);
            this.lblQueueStatus_1.Name = "lblQueueStatus_1";
            this.lblQueueStatus_1.Size = new System.Drawing.Size(0, 24);
            this.lblQueueStatus_1.TabIndex = 0;
            // 
            // lblMsg2_1
            // 
            this.lblMsg2_1.AutoSize = true;
            this.lblMsg2_1.Font = new System.Drawing.Font("Cambria", 8F);
            this.lblMsg2_1.ForeColor = System.Drawing.Color.Black;
            this.lblMsg2_1.Location = new System.Drawing.Point(99, 92);
            this.lblMsg2_1.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg2_1.Name = "lblMsg2_1";
            this.lblMsg2_1.Size = new System.Drawing.Size(9, 12);
            this.lblMsg2_1.TabIndex = 2;
            this.lblMsg2_1.Text = "-";
            // 
            // lblNowServing_1
            // 
            this.lblNowServing_1.AutoSize = true;
            this.lblNowServing_1.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNowServing_1.ForeColor = System.Drawing.Color.Black;
            this.lblNowServing_1.Location = new System.Drawing.Point(3, 49);
            this.lblNowServing_1.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblNowServing_1.Name = "lblNowServing_1";
            this.lblNowServing_1.Size = new System.Drawing.Size(112, 19);
            this.lblNowServing_1.TabIndex = 6;
            this.lblNowServing_1.Text = "Now Serving : ";
            // 
            // lblPreviousServe_1
            // 
            this.lblPreviousServe_1.AutoSize = true;
            this.lblPreviousServe_1.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreviousServe_1.ForeColor = System.Drawing.Color.Black;
            this.lblPreviousServe_1.Location = new System.Drawing.Point(5, 92);
            this.lblPreviousServe_1.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblPreviousServe_1.Name = "lblPreviousServe_1";
            this.lblPreviousServe_1.Size = new System.Drawing.Size(88, 12);
            this.lblPreviousServe_1.TabIndex = 5;
            this.lblPreviousServe_1.Text = "Previous Served : ";
            // 
            // lblMsg1_1
            // 
            this.lblMsg1_1.AutoSize = true;
            this.lblMsg1_1.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg1_1.ForeColor = System.Drawing.Color.Black;
            this.lblMsg1_1.Location = new System.Drawing.Point(115, 48);
            this.lblMsg1_1.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg1_1.Name = "lblMsg1_1";
            this.lblMsg1_1.Size = new System.Drawing.Size(16, 22);
            this.lblMsg1_1.TabIndex = 4;
            this.lblMsg1_1.Text = "-";
            // 
            // tmrBtnNext_4
            // 
            this.tmrBtnNext_4.Tag = global::QMS_Counter.Properties.Settings.Default.timer_4;
            this.tmrBtnNext_4.Tick += new System.EventHandler(this.tmrBtnNext_4_Tick);
            // 
            // pnlMain_1
            // 
            this.pnlMain_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(249)))), ((int)(((byte)(166)))));
            this.pnlMain_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMain_1.Controls.Add(this.lblMsg4_1);
            this.pnlMain_1.Controls.Add(this.lblMsg3_1);
            this.pnlMain_1.Controls.Add(this.lblDriverName_1);
            this.pnlMain_1.Controls.Add(this.lblCounterId_1);
            this.pnlMain_1.Controls.Add(this.btnNext_1);
            this.pnlMain_1.Controls.Add(this.lblNowServing_1);
            this.pnlMain_1.Controls.Add(this.pnlIndicator_1);
            this.pnlMain_1.Controls.Add(this.lblPreviousServe_1);
            this.pnlMain_1.Controls.Add(this.lblMsg2_1);
            this.pnlMain_1.Controls.Add(this.lblMsg1_1);
            this.pnlMain_1.Location = new System.Drawing.Point(340, 11);
            this.pnlMain_1.Name = "pnlMain_1";
            this.pnlMain_1.Size = new System.Drawing.Size(315, 156);
            this.pnlMain_1.TabIndex = 9;
            // 
            // lblMsg3_1
            // 
            this.lblMsg3_1.AutoSize = true;
            this.lblMsg3_1.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg3_1.ForeColor = System.Drawing.Color.Black;
            this.lblMsg3_1.Location = new System.Drawing.Point(77, 71);
            this.lblMsg3_1.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg3_1.Name = "lblMsg3_1";
            this.lblMsg3_1.Size = new System.Drawing.Size(9, 12);
            this.lblMsg3_1.TabIndex = 10;
            this.lblMsg3_1.Text = "-";
            // 
            // lblDriverName_1
            // 
            this.lblDriverName_1.AutoSize = true;
            this.lblDriverName_1.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDriverName_1.ForeColor = System.Drawing.Color.Black;
            this.lblDriverName_1.Location = new System.Drawing.Point(5, 71);
            this.lblDriverName_1.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblDriverName_1.Name = "lblDriverName_1";
            this.lblDriverName_1.Size = new System.Drawing.Size(72, 12);
            this.lblDriverName_1.TabIndex = 9;
            this.lblDriverName_1.Text = "Driver Name:";
            // 
            // lblCounterId_1
            // 
            this.lblCounterId_1.AutoSize = true;
            this.lblCounterId_1.Location = new System.Drawing.Point(277, 57);
            this.lblCounterId_1.Name = "lblCounterId_1";
            this.lblCounterId_1.Size = new System.Drawing.Size(35, 13);
            this.lblCounterId_1.TabIndex = 8;
            this.lblCounterId_1.Text = "label1";
            this.lblCounterId_1.Visible = false;
            // 
            // reset
            // 
            this.reset.BackColor = System.Drawing.Color.Red;
            this.reset.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reset.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.reset.Location = new System.Drawing.Point(554, 434);
            this.reset.Margin = new System.Windows.Forms.Padding(2);
            this.reset.Name = "reset";
            this.reset.Size = new System.Drawing.Size(77, 38);
            this.reset.TabIndex = 10;
            this.reset.Text = "RESET";
            this.reset.UseVisualStyleBackColor = false;
            this.reset.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblMsg4_1
            // 
            this.lblMsg4_1.AutoSize = true;
            this.lblMsg4_1.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg4_1.ForeColor = System.Drawing.Color.Black;
            this.lblMsg4_1.Location = new System.Drawing.Point(165, 52);
            this.lblMsg4_1.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg4_1.Name = "lblMsg4_1";
            this.lblMsg4_1.Size = new System.Drawing.Size(11, 15);
            this.lblMsg4_1.TabIndex = 11;
            this.lblMsg4_1.Text = "-";
            // 
            // lblMsg4_0
            // 
            this.lblMsg4_0.AutoSize = true;
            this.lblMsg4_0.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg4_0.ForeColor = System.Drawing.Color.Black;
            this.lblMsg4_0.Location = new System.Drawing.Point(165, 52);
            this.lblMsg4_0.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg4_0.Name = "lblMsg4_0";
            this.lblMsg4_0.Size = new System.Drawing.Size(11, 15);
            this.lblMsg4_0.TabIndex = 10;
            this.lblMsg4_0.Text = "-";
            // 
            // lblMsg4_2
            // 
            this.lblMsg4_2.AutoSize = true;
            this.lblMsg4_2.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg4_2.ForeColor = System.Drawing.Color.Black;
            this.lblMsg4_2.Location = new System.Drawing.Point(165, 52);
            this.lblMsg4_2.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg4_2.Name = "lblMsg4_2";
            this.lblMsg4_2.Size = new System.Drawing.Size(11, 15);
            this.lblMsg4_2.TabIndex = 11;
            this.lblMsg4_2.Text = "-";
            // 
            // lblMsg4_3
            // 
            this.lblMsg4_3.AutoSize = true;
            this.lblMsg4_3.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg4_3.ForeColor = System.Drawing.Color.Black;
            this.lblMsg4_3.Location = new System.Drawing.Point(165, 52);
            this.lblMsg4_3.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg4_3.Name = "lblMsg4_3";
            this.lblMsg4_3.Size = new System.Drawing.Size(11, 15);
            this.lblMsg4_3.TabIndex = 11;
            this.lblMsg4_3.Text = "-";
            // 
            // lblMsg4_4
            // 
            this.lblMsg4_4.AutoSize = true;
            this.lblMsg4_4.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg4_4.ForeColor = System.Drawing.Color.Black;
            this.lblMsg4_4.Location = new System.Drawing.Point(165, 52);
            this.lblMsg4_4.MaximumSize = new System.Drawing.Size(280, 0);
            this.lblMsg4_4.Name = "lblMsg4_4";
            this.lblMsg4_4.Size = new System.Drawing.Size(11, 15);
            this.lblMsg4_4.TabIndex = 11;
            this.lblMsg4_4.Text = "-";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(664, 510);
            this.Controls.Add(this.reset);
            this.Controls.Add(this.pnlMain_1);
            this.Controls.Add(this.pnlMain_4);
            this.Controls.Add(this.pnlMain_3);
            this.Controls.Add(this.pnlMain_2);
            this.Controls.Add(this.pnlMain_0);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Location = new System.Drawing.Point(300, 0);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = " ";
            this.TopMost = true;
            this.pnlIndicator_0.ResumeLayout(false);
            this.pnlIndicator_0.PerformLayout();
            this.pnlMain_0.ResumeLayout(false);
            this.pnlMain_0.PerformLayout();
            this.pnlIndicator_2.ResumeLayout(false);
            this.pnlIndicator_2.PerformLayout();
            this.pnlMain_2.ResumeLayout(false);
            this.pnlMain_2.PerformLayout();
            this.pnlIndicator_3.ResumeLayout(false);
            this.pnlIndicator_3.PerformLayout();
            this.pnlMain_3.ResumeLayout(false);
            this.pnlMain_3.PerformLayout();
            this.pnlIndicator_4.ResumeLayout(false);
            this.pnlIndicator_4.PerformLayout();
            this.pnlMain_4.ResumeLayout(false);
            this.pnlMain_4.PerformLayout();
            this.pnlIndicator_1.ResumeLayout(false);
            this.pnlIndicator_1.PerformLayout();
            this.pnlMain_1.ResumeLayout(false);
            this.pnlMain_1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnNext_0;
        private System.Windows.Forms.Panel pnlIndicator_0;
        private System.Windows.Forms.Label lblMsg2_0;
        private System.Windows.Forms.Timer tmrQuery;
        private System.Windows.Forms.Label lblQueueStatus_0;
        private System.Windows.Forms.Label lblMsg1_0;
        private System.Windows.Forms.Label lblPreviousServe_0;
        private System.Windows.Forms.Label lblNowServing_0;
        private System.Windows.Forms.Timer tmrBtnNext_0;
        private System.Windows.Forms.Panel pnlMain_0;
        private System.Windows.Forms.Button btnNext_2;
        private System.Windows.Forms.Panel pnlIndicator_2;
        private System.Windows.Forms.Label lblQueueStatus_2;
        private System.Windows.Forms.Label lblMsg2_2;
        private System.Windows.Forms.Label lblNowServing_2;
        private System.Windows.Forms.Label lblPreviousServe_2;
        private System.Windows.Forms.Label lblMsg1_2;
        private System.Windows.Forms.Timer tmrBtnNext_1;
        private System.Windows.Forms.Panel pnlMain_2;
        private System.Windows.Forms.Button btnNext_3;
        private System.Windows.Forms.Panel pnlIndicator_3;
        private System.Windows.Forms.Label lblQueueStatus_3;
        private System.Windows.Forms.Label lblMsg2_3;
        private System.Windows.Forms.Label lblNowServing_3;
        private System.Windows.Forms.Label lblPreviousServe_3;
        private System.Windows.Forms.Label lblMsg1_3;
        private System.Windows.Forms.Timer tmrBtnNext_2;
        private System.Windows.Forms.Panel pnlMain_3;
        private System.Windows.Forms.Button btnNext_4;
        private System.Windows.Forms.Panel pnlIndicator_4;
        private System.Windows.Forms.Label lblQueueStatus_4;
        private System.Windows.Forms.Label lblMsg2_4;
        private System.Windows.Forms.Label lblNowServing_4;
        private System.Windows.Forms.Label lblPreviousServe_4;
        private System.Windows.Forms.Label lblMsg1_4;
        private System.Windows.Forms.Timer tmrBtnNext_3;
        private System.Windows.Forms.Panel pnlMain_4;
        private System.Windows.Forms.Button btnNext_1;
        private System.Windows.Forms.Panel pnlIndicator_1;
        private System.Windows.Forms.Label lblQueueStatus_1;
        private System.Windows.Forms.Label lblMsg2_1;
        private System.Windows.Forms.Label lblNowServing_1;
        private System.Windows.Forms.Label lblPreviousServe_1;
        private System.Windows.Forms.Label lblMsg1_1;
        private System.Windows.Forms.Timer tmrBtnNext_4;
        private System.Windows.Forms.Panel pnlMain_1;
        private System.Windows.Forms.Label lblCounterId_0;
        private System.Windows.Forms.Label lblCounterId_2;
        private System.Windows.Forms.Label lblCounterId_3;
        private System.Windows.Forms.Label lblCounterId_4;
        private System.Windows.Forms.Label lblCounterId_1;
        private System.Windows.Forms.Button reset;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.Label lblDriverName_1;
        private System.Windows.Forms.Label lblMsg3_1;
        private System.Windows.Forms.Label lblMsg3_0;
        private System.Windows.Forms.Label lblDriverName_0;
        private System.Windows.Forms.Label lblMsg3_2;
        private System.Windows.Forms.Label lblDriverName_2;
        private System.Windows.Forms.Label lblMsg3_3;
        private System.Windows.Forms.Label lblDriverName_3;
        private System.Windows.Forms.Label lblMsg3_4;
        private System.Windows.Forms.Label lblDriverName_4;
        private System.Windows.Forms.Label lblMsg4_0;
        private System.Windows.Forms.Label lblMsg4_2;
        private System.Windows.Forms.Label lblMsg4_3;
        private System.Windows.Forms.Label lblMsg4_4;
        private System.Windows.Forms.Label lblMsg4_1;
    }
}

