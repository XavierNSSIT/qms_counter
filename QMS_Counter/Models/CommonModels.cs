﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS_Counter.Models
{
    public class QueueNoStatusMain
    {
        public string Counter { get; set; }
        public IList<QueueNoStatus> QueueNoStatus { get; set; }
    }

    public class QueueNoStatus
    {
        public string QueueType { get; set; }
        public int TotalQueue { get; set; }
    }

    public class NextQueueNo
    {
        public string QueueNo { get; set; }
        public string QueueType { get; set; }
        public string DriverName { get; set; }
        public string PlateNo { get; set; }
    }

    public class ServiceResult
    {
        public string Status { get; set; }

        public string ErrorMessage { get; set; }

    }
}
